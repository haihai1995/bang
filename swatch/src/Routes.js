import React from 'react';
import Notfound from './components/Notfound.component';

const Routes = [
  {
    path : '/',
    exact : false,
    main : () => <Notfound />
  }
]

export default Routes;