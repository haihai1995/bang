import React, { Component } from 'react';
import './order.css';

export default class OrderDetail extends Component {
    render() {
        return (
            <div className="order-detail">
                <h4>{this.props.productName}</h4>
                <p>Price: {this.props.price} USD</p>
                <p>Quantity: {this.props.quantity}</p>
                <p>
                    <button onClick={this.props.addHandler}>+</button>
                    <button onClick={this.props.UnAddHandler}>-</button>
                </p>
            </div>
        )
    }
}
