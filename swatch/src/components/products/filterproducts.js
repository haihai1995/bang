import React, { Component } from 'react';
import '../../css/filterproducts.css';
import Allfilter from './allfilter';
import Apple from './apple';
import Samsung from './samsung';
import Xiaomi from './xiaomi';
import Huawei from './huawei';
import Up from './upDown';
import New from '../newproduct/new';
import Slide from './../layers/Slide';

export default class Filterproducts extends Component {

    render() {
        return (
            <div>
                <Slide />
                <New />
                <div className="container"><hr />
                    <div className="text-center">
                        <h3>SEE ALL PRODUCTS</h3>
                    </div>
                </div>
                <div className="container">
                    <div className="border nav nav-pills bg-secondary" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <button className="nav-link border active stylefilter btn-sm border-secondary hoverButton" id="v-pills-featured-tabs" data-toggle="pill"
                            href="#v-pills-featureds"
                            role="tab" aria-controls="v-pills-featureds" aria-selected="true">All products</button>
                        <button className="nav-link border stylefilter btn-sm border-secondary hoverButton" id="v-pills-featured-tab" data-toggle="pill"
                            href="#v-pills-featured"
                            role="tab" aria-controls="v-pills-featured" aria-selected="true">Apple</button>
                        <button className="nav-link border stylefilter btn-sm border-secondary hoverButton" id="v-pills-product-tab" data-toggle="pill"
                            href="#v-pills-product"
                            role="tab" aria-controls="v-pills-product" aria-selected="false">Samsung</button>
                        <button className="nav-link border stylefilter btn-sm border-secondary hoverButton" id="v-pills-business-tab" data-toggle="pill"
                            href="#v-pills-business" role="tab" aria-controls="v-pills-business"
                            aria-selected="false">Xiaomi</button>
                        <button className="nav-link border stylefilter btn-sm border-secondary hoverButton" id="v-pills-huawei-tab" data-toggle="pill"
                            href="#v-pills-huawei" role="tab" aria-controls="v-pills-huawei"
                            aria-selected="false">Huawei</button>
                        <button className="nav-link border stylefilter btn-sm border-secondary buttonleft hoverButton" id="v-pills-filter-tab" data-toggle="pill"
                            href="#v-pills-filter" role="tab" aria-controls="v-pills-filter"
                            aria-selected="false">Advanced filtering</button>
                    </div>

                    <div className="container border tab-content" id="v-pills-tabContent">
                        <div className="tab-pane fade show active" id="v-pills-featureds" role="tabpanel"
                            aria-labelledby="v-pills-featured-tabs">
                            <Allfilter />
                        </div>
                        <div className="tab-pane fade" id="v-pills-featured" role="tabpanel"
                            aria-labelledby="v-pills-featured-tab">
                            <Apple />
                        </div>
                        <div className="tab-pane fade" id="v-pills-product" role="tabpanel"
                            aria-labelledby="v-pills-product-tab">
                            <Samsung />
                        </div>
                        <div className="tab-pane fade" id="v-pills-business" role="tabpanel"
                            aria-labelledby="v-pills-business-tab">
                            <Xiaomi />
                        </div>
                        <div className="tab-pane fade" id="v-pills-huawei" role="tabpanel"
                            aria-labelledby="v-pills-huawei-tab">
                            <Huawei />
                        </div>
                        <div className="tab-pane fade" id="v-pills-filter" role="tabpanel"
                            aria-labelledby="v-pills-filter-tab">
                            <Up />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
