import React from 'react';
import { Link } from "react-router-dom";

export default class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            persons: [],
            visible: 4,
            error: false,
            query: "",
            filteredData: [],
        };
    };

    loadMore = () => {
        this.setState((prev) => {
            return { visible: prev.visible + 4 };
        });
    }

    handleInputChange = event => {
        const query = event.target.value;

        this.setState(prevState => {
            const filteredData = prevState.persons.filter(element => {
                return element.name.toLowerCase().includes(query.toLowerCase());
            });

            return {
                query,
                filteredData
            };
        });
    };

    getData = () => {
        fetch(`http://localhost:4000/persons`)
            .then(response => response.json())
            .then(persons => {
                const { query } = this.state;
                const filteredData = persons.filter(element => {
                    return element.name.toLowerCase().includes(query.toLowerCase());
                });

                this.setState({
                    persons,
                    filteredData
                });
            });
    };

    componentWillMount() {
        this.getData();
    }

    render() {
        return (
            <div className="container">
                <form className="ml-md-3">
                    <div className="form-group text-left bg-light border-top border-bottom p-2 mt-2 mb-2 row">
                        <label htmlFor="inputName" className="col-sm-auto col-form-label">Search for product names</label>
                        <div className="col-sm-9">
                            <input type="name"
                                className="form-control"
                                autoComplete="on" autoFocus
                                id="inputName"
                                placeholder="Search ..."
                                value={this.state.query}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>
                </form>

                <div className="row">
                    {this.state.filteredData.slice(0, this.state.visible).map((i, index) =>
                        <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12 mb-md-5 mb-sm-2" key={index}>
                            <div className="thumbnail">
                                <Link to={"/detail/" + i._id}>
                                    <img className="w-100 border"
                                        src={i.image}
                                        alt="" />
                                </Link>
                            </div>
                            <div className="caption">
                                <span>
                                    <Link to={"/detail/" + i._id}>
                                        <b className="size-name text-dark">{i.name}</b><br />
                                    </Link>
                                    <span>
                                        <i className="old-price"><strike>{(i.price).toLocaleString()} vnđ.</strike></i>
                                        <b className="price text-danger">  {(i.sale).toLocaleString()} vnđ.</b>
                                    </span>
                                </span><br />
                            </div>
                        </div>
                    )}
                </div>
                <div className="text-center p-3">
                    {this.state.visible < this.state.persons.length &&
                        <button onClick={this.loadMore} type="button" className="load-more btn btn-success dropdown-toggle">See more </button>
                    }
                </div>
            </div>
        )
    }
}
