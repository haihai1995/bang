import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleUp, faArrowCircleDown, faArrowsAlt, faNewspaper } from '@fortawesome/free-solid-svg-icons';

export default class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            persons: [],
            visible: 8,
            error: false,
            query: '',
            querys: 'new',
            filteredData: [],
        };
    };

    loadMore = () => {
        this.setState((prev) => {
            return { visible: prev.visible + 4 };
        });
    }

    buff = () => {
        fetch(`http://localhost:4000/persons`)
            .then(response => response.json())
            .then(persons => {
                const filteredData = persons.sort((element, index) => {
                    return index.price - element.price;
                });

                this.setState({
                    persons,
                    filteredData
                });
            });
    }

    nerf = () => {
        fetch(`http://localhost:4000/persons`)
            .then(response => response.json())
            .then(persons => {
                const filteredData = persons.sort((element, index) => {
                    return element.price - index.price;
                });

                this.setState({
                    persons,
                    filteredData
                });
            });
    }

    getData = () => {
        fetch(`http://localhost:4000/persons`)
            .then(response => response.json())
            .then(persons => {
                const { query } = this.state;
                const filteredData = persons.filter(element => {
                    return element.manufacturer.toLowerCase().includes(query.toLowerCase());
                });

                this.setState({
                    persons,
                    filteredData
                });
            });
    };

    componentWillMount() {
        this.getData();
    };

    news = () => {
        fetch(`http://localhost:4000/persons`)
            .then(response => response.json())
            .then(persons => {
                const { querys } = this.state;
                const filteredData = persons.filter(element => {
                    return element.np.toLowerCase().includes(querys.toLowerCase());
                });

                this.setState({
                    persons,
                    filteredData
                });
            });
    }

    render() {
        const arrows = <FontAwesomeIcon icon={faArrowsAlt} />
        const up = <FontAwesomeIcon icon={faArrowCircleUp} />
        const down = <FontAwesomeIcon icon={faArrowCircleDown} />
        const news = <FontAwesomeIcon icon={faNewspaper} />
        return (
            <div><hr />
                <div className="dropdown container text-center">
                    <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Sorted by price
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <button className="btn" onClick={this.getData}>Popularity {arrows}</button><br />
                        <button className="btn" onClick={this.buff}>Price from high to low {down}</button><br />
                        <button className="btn" onClick={this.nerf}>Price from low to high {up}</button>
                        <div className="dropdown-divider"></div>
                        <button className="btn" onClick={this.news}>New product launches {news}</button>
                    </div>
                </div>
                <div className="row container" style={{ margin: "auto" }}>
                    {this.state.filteredData.slice(0, this.state.visible).map((i, index) =>
                        <div className="item col-xs-6 col-lg-3" key={index}><br />
                            <div className="thumbnail">
                                <img className="group list-group-image img-thumbnail" src={i.image} alt={i.name} />
                                <div className="caption group-sp" style={{ backgroundColor: "#FAFAD2", border: "2px solid #EEE8AA" }}>
                                    <h6 className="group inner list-group-item-heading"> {i.name} </h6>
                                    <div className="row">
                                        <div className="col-6">
                                            <p className="group inner list-group-item-text"> <del> {(i.price).toLocaleString()} </del><sup>đ</sup></p>
                                        </div>
                                        <div className="col-6">
                                            <p className="group inner list-group-item-text">
                                                {i.amounts > 0 ?
                                                    <span><img className="img-responsive" style={{ width: 20 }} src="/Images/check.png" alt={i.status} /><b> Còn hàng</b></span> :
                                                    <span><img className="img-responsive" style={{ width: 20 }} src="/Images/checks.png" alt={i.status} /><b> Hết hàng</b></span>}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-12 col-md-7">
                                            <p className="lead text-danger"><b> {(i.sale).toLocaleString()}<sup>đ</sup> </b></p>
                                        </div>
                                        <div className="col-xs-12 col-md-5">
                                            <Link to={"/detail/" + i._id} className="btn btn-primary btn-sm">Details</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
                <div className="text-center p-3">
                    {this.state.visible < this.state.persons.length &&
                        <button onClick={this.loadMore} type="button" className="load-more btn btn-success dropdown-toggle">See more </button>
                    }
                </div>
            </div>
        )
    }
}
