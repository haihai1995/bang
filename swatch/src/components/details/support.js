import React, { Component } from 'react';
import Axios from 'axios';
import swal from '@sweetalert/with-react';

export default class Support extends Component {
    constructor() {
        super();

        let today = new Date();

        this.state = {
            advisory: '',
            date: today
        }
    }

    onChangeAdvisory = (e) => {
        this.setState({
            advisory: e.target.value
        })
    }

    send = () => {
        if (this.state.advisory === '') {
            swal("Vui lòng nhập số điện thoại !", "Warning ...", "warning");
        } else if (this.state.advisory.length === 10) {
            swal("Bộ phận chăm sóc khách hàng sẽ gọi lại bạn sau ít phút !", "Loading ...", "success");
        } else {
            swal("Vui lòng kiểm tra lại số điện thoại bạn đã nhập", "Warning ...", "warning");
        }
    }

    onSubmits = (e) => {
        e.preventDefault();

        const obj = {
            advisory: this.state.advisory,
            date: this.state.date
        };
        Axios.post('http://localhost:4000/supports/add', obj)
            .then(res => {
                localStorage.setItem('tasks', JSON.stringify(res));
                console.log(res.data);
            }).catch(err => {
                console.log(err);
            });

        this.setState({
            advisory: '',
            today: []
        });
    };

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmits}>
                    <div className="container">
                        <div className="row bg-dark">
                            <div className="container"><br />
                                <h5 className="text-white">Free Consultation</h5>
                            </div><br />
                            <div className="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                <div className="form-group">
                                    <input type="text" className="form-control" maxLength="10"
                                        placeholder="Để lại số điện thoại" required step="0"
                                        pattern=".{10,}" title="vui long nhap dung sdt"
                                        value={this.state.advisory}
                                        onChange={this.onChangeAdvisory} />
                                </div><br />
                            </div>
                            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <div className="form-group text-right">
                                    <button type="submit" className="btn btn-danger" onClick={this.send}>Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
