import React, { Component } from 'react';
import swal from '@sweetalert/with-react';
import axios from 'axios';
import Iframe from 'react-iframe';
import ReactPlayer from 'react-player';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarker } from '@fortawesome/free-solid-svg-icons';
import Support from './support';

export default class DetailProduct extends Component {
    constructor(props) {
        super(props);

        this.state = { persons: [] };

        let today = new Date();

        this.state = {
            name: '',
            status: '',
            check: '',
            amounts: 0,
            price: '',
            image: '',
            detail: '',
            sale: '',
            color: '',
            color1: '',
            color2: '',
            gift: '',
            gifts: '',
            promotionone: '',
            promotiontwo: '',
            manufacturer: '',
            screen: '',
            pin: '',
            ram: '',
            rom: '',
            cpu: '',
            pk: '',
            waterproof: '',
            material: '',
            guarantee: '',
            guarantees: '',
            image_src: '',
            video: '',
            quantity: 1,
            description: '',

            nickname: '',
            phone: '',
            email: '',
            address: '',
            note: '',
            amount: 0,
            date: today,
            totalPrice: '',

            id: ''
        };
    }

    notification = () => {
        if (this.state.amount === "") {
            swal("vui lòng điền số lượng sản phẩm !", "Warning ...", "warning");
        } else if (this.state.nickname === "") {
            swal("Vui lòng điền đầy đủ tên !", "Warning ...", "warning");
            return false;
        } else if (this.state.phone === "" || this.state.phone.length < 10) {
            swal("Vui lòng điền đầy đủ số điện thoại !", "Warning ...", "warning");
            return false;
        } else if (this.state.address === "") {
            swal("Vui lòng ghi rõ địa chỉ nơi nhận hàng !", "Warning ...", "warning");
        } else {
            setTimeout((e) => {
                this.props.history.push('/bill');
                window.location.reload();
            }, 2500);
            swal("Successful purchases", "Chúc mừng bạn đã đặt hàng thành công !", "success");
        }
    }

    onChangeNickname = (e) => {
        this.setState({
            nickname: e.target.value
        });
    }

    onChangePhone = (e) => {
        this.setState({
            phone: e.target.value
        });
    }

    onChangeEmail = (e) => {
        this.setState({
            email: e.target.value
        });
    }

    onChangeAddress = (e) => {
        this.setState({
            address: e.target.value
        });
    }

    onChangeNote = (e) => {
        this.setState({
            note: e.target.value
        });
    }

    onChangeAmounts = (e) => {
        this.setState({
            amount: e.target.value,
            amounts: e.target.value
        });
    }

    onChangeColor = (e) => {
        this.setState({
            color: e.target.value,
            color1: e.target.value,
            color2: e.target.value
        })
    }

    handleTotal = (e) => {
        this.setState({
            totalPrice: e.target.value
        })
    }

    onSubmit = (e) => {
        e.preventDefault();

        const obj = {
            nickname: this.state.nickname,
            phone: this.state.phone,
            email: this.state.email,
            address: this.state.address,
            note: this.state.note,
            name: this.state.name,
            amount: this.state.amount,
            color: this.state.color,
            price: this.state.price,
            sale: this.state.sale,
            image: this.state.image,
            date: this.state.date,
            totalPrice: this.state.totalPrice,
            id: this.state.id
        };
        axios.post('http://localhost:4000/orders/add', obj)
            .then(res => {
                console.log(res.data);
                localStorage.setItem('reciept-1800.9999-order', res.config.data);

            }).catch(err => {
                console.log(err);
            })

        this.setState({
            nickname: '',
            phone: '',
            email: '',
            address: '',
            name: '',
            note: '',
            amount: '',
            color: '',
            price: '',
            sale: '',
            image: '',
            date: [],
            totalPrice: '',

            id: ''
        });

        // update data
        const updateData = {
            check: this.state.check,
            status: this.state.status,
            date: this.state.date,
            amounts: this.state.amounts,
            name: this.state.name,
            price: this.state.price,
            sale: this.state.sale,
            image: this.state.image,
            image_src: this.state.image_src,
            detail: this.state.detail,
            gift: this.state.gift,
            gifts: this.state.gifts,
            promotion: this.state.promotion,
            promotions: this.state.promotions,
            pk: this.state.pk,
            guarantee: this.state.guarantee,
            guarantees: this.state.guarantees,

            manufacturer: this.state.manufacturer,
            color: this.state.color,
            screen: this.state.screen,
            ram: this.state.ram,
            rom: this.state.rom,
            cpu: this.state.cpu,
            pin: this.state.pin,
            waterproof: this.state.waterproof,
            material: this.state.material
        };
        axios.post('http://localhost:4000/persons/update/' + this.props.match.params.id, updateData)
            .then(res => console.log(res.data));
    }

    componentDidMount() {
        axios.get('http://localhost:4000/persons/detail/' + this.props.match.params.id)
            .then(res => {
                this.setState({
                    _id: res.data._id,
                    name: res.data.name,
                    status: res.data.status,
                    check: res.data.check,
                    amounts: res.data.amounts,
                    price: res.data.price,
                    image: res.data.image,
                    sale: res.data.sale,
                    color: res.data.color,
                    color1: res.data.color1,
                    color2: res.data.color2,
                    gift: res.data.gift,
                    gifts: res.data.gifts,
                    promotionone: res.data.promotionone,
                    promotiontwo: res.data.promotiontwo,
                    manufacturer: res.data.manufacturer,
                    screen: res.data.screen,
                    pin: res.data.pin,
                    ram: res.data.ram,
                    rom: res.data.rom,
                    cpu: res.data.cpu,
                    pk: res.data.pk,
                    waterproof: res.data.waterproof,
                    material: res.data.material,
                    guarantee: res.data.guarantee,
                    guarantees: res.data.guarantees,
                    image_src: res.data.image_src,
                    detail: res.data.detail,
                    video: res.data.video,
                    description: res.data.description
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    render() {
        const total = this.state.sale * this.state.amount;
        const map = <FontAwesomeIcon icon={faMapMarker} />
        return (
            <div className="container"><hr />
                <p>Trang chủ > Chi tiết sản phẩm > <b>{this.state.name}</b></p>
                <br />
                <div className="row">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div className="containers text-center">
                            <img className="group list-group-image img-thumbnail" src={this.state.image} alt={this.state.name} />
                            <Iframe className="iframe-fb" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=450&layout=standard&action=like&size=small&share=true&height=35&appId=728127094590611"
                                style={{ border: 'none', overflow: 'hidden', height: 45, width: 350 }} scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></Iframe>
                        </div>
                        <div className="navbar navbar-expand-lg navbar-light">
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                <i>Chi tiết thông số kỹ thuật</i>
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarNavDropdown">
                                <ul className="list-group">
                                    <li className="list-group-item active bg-secondary"><h5>Thông số kỹ thuật</h5></li>
                                    <li className="list-group-item">Hãng sản xuất: <b>{this.state.manufacturer}</b></li>
                                    <li className="list-group-item">Màn hình: <b>{this.state.screen}</b></li>
                                    <li className="list-group-item">Chất liệu: <b>{this.state.material}</b></li>
                                    <li className="list-group-item">RAM/ROM: <b>{this.state.ram} / {this.state.rom}</b></li>
                                    <li className="list-group-item">CPU: <b>{this.state.cpu}</b></li>
                                    <li className="list-group-item">Chống nước: <b>{this.state.waterproof}</b></li>
                                    <li className="list-group-item">Cổng sạc: <b>{this.state.detail}</b></li>
                                    <li className="list-group-item">Pin: <b>{this.state.pin}</b></li>
                                </ul>
                            </div>
                        </div><br />
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <h4> {this.state.name} </h4>
                        <div className="row">
                            <div className="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <p>Giá sản phẩm: </p>
                                <h5 className="text-right"><b className="text-center"><del>{(this.state.price).toLocaleString()}</del><sup>vnđ</sup> </b></h5>
                                <h2 className="text-right"><b className="text-danger">{(this.state.sale).toLocaleString()}<sup>vnđ</sup> </b></h2>
                            </div>
                            <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right">
                                <button type="button" className="btn btn-danger">TRẢ GÓP 0%</button>
                            </div>
                            <p className="container">
                                {this.state.amounts > 0 ?
                                    <span><img src="/Images/check.png" style={{ width: 20 }} className="img-responsive" alt={this.state.status} /><b> Còn hàng</b></span> :
                                    <span><img src="/Images/checks.png" style={{ width: 20 }} className="img-responsive" alt={this.state.status} /><b> Hết hàng</b></span>}
                            </p>
                        </div>
                        <ul className="list-group">
                            <li className="list-group-item active bg-secondary"> <b> KHUYẾN MÃI </b> </li>
                            <li className="list-group-item">{this.state.promotionone}</li>
                            <li className="list-group-item">{this.state.promotiontwo}</li>
                        </ul><br />
                        <ul className="list-group">
                            <li className="list-group-item active bg-secondary"> <b> TẶNG </b> </li>
                            <li className="list-group-item">{this.state.gift}</li>
                            <li className="list-group-item">{this.state.gifts}</li>
                        </ul><br />
                        <div className="container">
                            <div className="text-center">
                                {this.state.amounts > 0 ?
                                    <button type="button" className="btn btn-danger" data-toggle="modal" data-target="#myModal">ĐẶT HÀNG NGAY <br /> (Giao hàng nhanh hoặc lấy tại cửa hàng)</button> :
                                    <button type="button" className="btn btn-secondary" onClick={this.onAlert = () => { swal("Sản phẩm hết hàng !", "Quý khách cập website thường xuyên để biết thông tin hàng về hoặc liên hệ 1800.6666", "warning"); }}>HẾT HÀNG <br /> (Liên hệ để cập nhật thông tin 19006666)</button>
                                }
                            </div>
                            <div id="myModal" className="modal fade" role="dialog">
                                <div className="modal-dialog">
                                    <div className="modal-content">
                                        <div className="bg-secondary">
                                            <div className="container">
                                                <form onSubmit={this.onSubmit}><br />
                                                    <button type="button" className="close text-white" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times; &nbsp;</span>
                                                    </button>
                                                    <h4 className="text-white">Thông tin sản phẩm</h4><br />
                                                    <div className="row">
                                                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                            <img src={this.state.image} value={this.state.image} className="img-responsive" style={{ width: 150 }} alt={this.state.name} />
                                                        </div>
                                                        <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                            <div className="row">
                                                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                    <p className="text-white">()Tên sản phẩm</p>
                                                                    <h6 value={this.state.name} className="text-white">{this.state.name}</h6>
                                                                    <h6 value={this.state.price}> <del>{(this.state.price).toLocaleString()}</del><sup>vnđ</sup> </h6>
                                                                    <h4><b className="text-danger">{(this.state.sale).toLocaleString()}<sup>vnđ</sup> </b></h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr className="bg-white" />
                                                        <div className="col-6">
                                                            <p className="text-white">(*)Màu sắc</p>
                                                            <div className="col-7" style={{ backgroundColor: this.state.color }}>
                                                                <select className="form-control bg-white" value={this.state.color} onChange={this.onChangeColor}>
                                                                    <option style={{ backgroundColor: this.state.color }} value={this.state.color}> {this.state.color} </option>
                                                                    <option style={{ backgroundColor: this.state.color1 }} value={this.state.color1}>{this.state.color1}</option>
                                                                    <option style={{ backgroundColor: this.state.color2 }} value={this.state.color2}>{this.state.color2}</option>
                                                                </select>
                                                            </div>
                                                            <p className="text-white">(*)Số lượng:
                                                            <span className="text-white" style={{ fontSize: 20 }} value={this.state.amount}> <b className="text-danger">{this.state.amount}</b> </span>
                                                            </p>
                                                            <button type="button" value={this.state.amounts} onChange={this.onChangeAmounts} className="btn btn-sm btn-light" onClick={this.ins = (e) => {
                                                                this.setState(() => {
                                                                    if (this.state.amount < 4) {
                                                                        return {
                                                                            amount: this.state.amount + 1,
                                                                            amounts: this.state.amounts - 1
                                                                        }
                                                                    } else {
                                                                        return swal("Max 4", "Warning", "warning");
                                                                    }
                                                                })
                                                            }}>+</button> &nbsp;
                                                            <button type="button" className="btn btn-sm btn-light" onClick={this.del = (e) => {
                                                                this.setState(() => {
                                                                    if (this.state.amount > 1) {
                                                                        return {
                                                                            amount: this.state.amount - 1
                                                                        }
                                                                    } else return swal("Min 1", "Warning", "warning");
                                                                })
                                                            }}>-</button>
                                                        </div>
                                                        <div className="col-6" style={{ marginTop: '7%' }}>
                                                            <p className="text-white">(*)Tổng đơn hàng:</p>
                                                            <span className="text-white" style={{ float: 'right' }}
                                                                value={this.state.totalPrice = total}
                                                                onChange={this.handleTotal}>
                                                                <b className="text-danger text-right" style={{ fontSize: 25 }}>{(this.state.sale * this.state.amount).toLocaleString()}</b> vnđ.
                                                            </span>
                                                            <div className="row container">
                                                                <p className="text-white">Đã bao gồm phí VAT 10%</p>
                                                            </div>
                                                        </div>
                                                    </div><br />

                                                    <h4 className="text-white">Thông tin khách hàng</h4>
                                                    <div className="row">
                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className="text-white">(*) HỌ & TÊN</label>
                                                                <input type="text" className="form-control" placeholder="..." autoFocus
                                                                    pattern="[A-Za-z /-,]{1,100}" title="vui lòng ghi rõ họ tên" required
                                                                    value={this.state.nickname}
                                                                    onChange={this.onChangeNickname} />
                                                            </div>
                                                            <div className="form-group">
                                                                <label className="text-white">(*) SỐ ĐIỆN THOẠI</label>
                                                                <input type="text" className="form-control" maxLength="10" placeholder="+84"
                                                                    pattern=".{10,}" title="vui lòng điền đúng số điện thoại" required
                                                                    value={this.state.phone}
                                                                    onChange={this.onChangePhone} />
                                                            </div>
                                                        </div>
                                                        <div className="col-6">
                                                            <div className="form-group">
                                                                <label className="text-white">() EMAIL</label>
                                                                <input type="email" name="email" className="form-control" autoComplete="on" placeholder="...@email.com.vn"
                                                                    value={this.state.email}
                                                                    onChange={this.onChangeEmail} />
                                                            </div>
                                                            <div className="form-group">
                                                                <label className="text-white">() GHI CHÚ</label>
                                                                <input type="text" className="form-control" placeholder="..."
                                                                    value={this.state.note}
                                                                    onChange={this.onChangeNote} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="text-white">(*) ĐỊA CHỈ</label>
                                                        <input type="text" className="form-control"
                                                            pattern="[A-Za-z /-,]{1,100}" title="vui lòng điền đúng địa chỉ !"
                                                            placeholder="Địa chỉ" required
                                                            value={this.state.address}
                                                            onChange={this.onChangeAddress} />
                                                    </div>
                                                    <div className="form-group text-center">
                                                        <button type="submit" className="btn btn-danger" onClick={this.notification}><h5>ĐẶT HÀNG</h5> ( Swatch sẽ gọi sau ít phút đến quý khách. )</button>
                                                    </div>
                                                    <p className="text-white">Lưu ý: (*) bắt buộc. () không bắt buộc.</p>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><br />
                        <div className="text-center">
                            <button type="button" className="btn btn-primary text-center">TRẢ GÓP 0% <br /> ( Xét duyệt qua điện thoại, Tablet, laptop )</button>
                        </div><br />
                        <div className="text-center">
                            <button type="button" className="btn btn-success text-center">TRẢ GÓP 0% QUA THẺ <br /> ( Visa, Master Card, JCB, ATM ngân hàng )</button>
                        </div><br />
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <ul className="list-group">
                            <li className="list-group-item active bg-secondary">{map}<b>&nbsp; 24 Nguyễn Cơ Trạch - Mỹ Đình 1 - Hà Nội - Viêt Nam. </b> </li>
                        </ul><br />
                        <ul className="list-group">
                            <li className="list-group-item active bg-danger"> <b> BẢO HÀNH & HẬU MÃI </b> </li>
                            <li className="list-group-item">Bảo hành chính hãng 12 tháng ( nguồn, màn hình, phần mềm )</li>
                            <li className="list-group-item">1 đổi 1 trong vòng 30 ngày ( tính từ thời điểm kích hoạt bảo hành )</li>
                            <li className="list-group-item">Bảo hành miễn phí phần mềm trọn đời</li>
                        </ul><br />
                        <ul className="list-group">
                            <li className="list-group-item active bg-secondary"> <b> FULL BOX </b> </li>
                            <li className="list-group-item">Trong hộp có: {this.state.pk}</li>
                            <li className="list-group-item">{this.state.guarantee}</li>
                            <li className="list-group-item">{this.state.guarantees}</li>
                        </ul><br />
                        <Support /><br /><hr />
                        <p><b className="text-danger">*Attention: </b>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <h3>Evaluate</h3>
                        <div id="accordion">
                            <div className="card">
                                <div className="card-header" id="headingOne">
                                    <h5 className="mb-0">
                                        <button className="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Evaluate post
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div className="card-body">
                                        <p>{this.state.description}</p>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header" id="headingTwo">
                                    <h5 className="mb-0">
                                        <button className="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Evaluate video
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div className="card-body">
                                        <ReactPlayer url={this.state.video} controls={true} />
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header" id="headingThree">
                                    <h5 className="mb-0">
                                        <button className="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Evaluate use
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div className="card-body">
                                        <div className="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5" data-width="1080"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br />
                <div className="row">
                    <div className="col-12">
                        <h3>Comment</h3>
                        <div className="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5" data-width="1080"></div>
                    </div>
                </div>
            </div>
        )
    }
}

