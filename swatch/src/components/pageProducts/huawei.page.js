import React, {Component} from 'react';
import Huawei from '../products/huawei';

export default class Applepage extends Component {
    render() {
        return (
            <div className="container">
                <hr/>
                <h3 className="text-center"><strong>Huawei products</strong></h3>
                <hr/>
                <Huawei />
            </div>
        )
    }
}
