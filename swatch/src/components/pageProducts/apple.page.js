import React from 'react';
import Apple from '../products/apple';

export default class Applepage extends React.Component {
    render() {
        return (
            <div className="container">
                <hr/>
                <h3 className="text-center"><strong>Apple products</strong></h3>
                <hr/>
                <Apple />
            </div>
        )
    }
}
