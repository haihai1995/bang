import React, {Component} from 'react';
import Samsung from '../products/samsung';

export default class Samsungpage extends Component {
    render() {
        return (
            <div className="container">
                <hr/>
                <h3 className="text-center"><strong>Samsung products</strong></h3>
                <hr/>
                <Samsung />
            </div>
        )
    }
}
