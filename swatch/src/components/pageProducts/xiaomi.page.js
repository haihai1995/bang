import React, {Component} from 'react';
import Xiaomi from '../products/xiaomi';

export default class Applepage extends Component {
    render() {
        return (
            <div className="container">
                <hr/>
                <h3 className="text-center"><strong>Xiaomi products</strong></h3>
                <hr/>
                <Xiaomi />
            </div>
        )
    }
}
