import React, { Component } from 'react';
import Axios from 'axios';
import Tableproduct from './Table.product';
import Promotion from '../promotionTime/promotion';

export default class New extends Component {
    constructor(props) {
        super(props);
        this.state = {
            persons: []
        };
    };

    componentDidMount() {
        Axios.get('http://localhost:4000/persons')
            .then(response => {
                // console.log(response.data);
                this.setState({ persons: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    tabRow() {
        return this.state.persons.map(function (object, i) {
            return <Tableproduct obj={object} key={i} />;
        });
    }

    render() {
        return (
            <div className="container"><br />
                <div className="bg-mured border">
                    <h2 className="text-center"><strong>SẢN PHẨM MỚI + KHUYẾN MÃI</strong></h2>
                    <Promotion />
                    <div id="recipeCarousel" className="carousel slide w-100" data-ride="carousel">
                        <div className="carousel-inner w-100" role="listbox">
                            <div className="d-flex carousel-item row no-gutters active">
                                {this.tabRow()}
                            </div>
                        </div>
                        <a className="carousel-control-prev" href="#recipeCarousel" role="button" data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#recipeCarousel" role="button" data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}
