import React, { Component } from 'react';

export default class Tableproduct extends Component {

    render() {
        const { name, image, price, sale } = this.props.obj;
        return (
            <div className="col-3 col-md-3">
                <div className="card mb-2">
                    <img className="pull-right position-absolute" style={{ width: "30%", marginLeft: "70%" }} src="./Images/new.png" alt="new" />
                    <img className="pull-right position-absolute" style={{ width: "30%", marginLeft: "70%", marginTop: "39%" }} src="./Images/sale.jpg" alt="new" />
                    <img className="card-img-top" style={{ width: "70%" }} src={image} alt={name} />
                    <div className="card-body bg-secondary">
                        <h6 className="card-title font-weight-bold text-white">{name}</h6>
                        <h6><del>{price}</del> vnđ</h6>
                        <h5 className="text-danger"><b>{sale}</b> vnđ</h5>
                    </div>
                </div>
            </div>
        );
    }
}
