import React, { Component } from 'react';
import { Link } from "react-router-dom";
import PromotionTime from '../promotionTime/promotion';

export default class New extends Component {
    constructor(props) {
        super(props);
        this.state = {
            persons: [],
            visible: 4,
            error: false,
            query: "new",
            filteredData: [],
        };
    };

    loadMore = () => {
        this.setState((prev) => {
            return { visible: prev.visible + 4 };
        });
    }

    getData = () => {
        fetch(`http://localhost:4000/persons`)
            .then(response => response.json())
            .then(persons => {
                const { query } = this.state;
                const filteredData = persons.filter(element => {
                    return element.np.toLowerCase().includes(query.toLowerCase());
                });

                this.setState({
                    persons,
                    filteredData
                });
            });
    };

    componentWillMount() {
        this.getData();
    }

    render() {
        return (
            <div>
                <div className="container"><hr />
                    <div className="text-center">
                        <h3>NEW PRODUCTS & SALE</h3>
                        <div className="bg-danger text-white" style={{ fontSize: 20 }}>
                            <PromotionTime />
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        {this.state.filteredData.slice(0, this.state.visible).map((i, index) =>
                            <div className="item col-xs-6 col-lg-3" key={index}><br />
                                <div className="thumbnail">
                                    <Link to={"/detail/" + i._id} style={{ textDecoration: "none" }}>
                                        <div className="card mb-2">
                                            <img className="pull-right position-absolute" style={{ width: "30%", marginLeft: "70%" }} src="./Images/new.png" alt="new" />
                                            <img className="pull-right position-absolute" style={{ width: "30%", marginLeft: "70%", marginTop: "39%" }} src="./Images/sale.jpg" alt="new" />
                                            <img className="card-img-top" style={{ width: "70%" }} src={i.image} alt={i.name} />
                                            <div className="card-body" style={{ backgroundColor: "#FAFAD2", border: "2px solid #EEE8AA" }}>
                                                <h6 className="card-title font-weight-bold text-body">{i.name}</h6>
                                                <h6 className="text-dark"><del>{(i.price).toLocaleString()}</del> vnđ</h6>
                                                <h5 className="text-danger"><b>{(i.sale).toLocaleString()}</b> vnđ</h5>
                                            </div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        )}
                    </div>
                    <div className="text-center p-3">
                        {this.state.visible < this.state.persons.length &&
                            <button onClick={this.loadMore} type="button" className="load-more btn btn-success dropdown-toggle">See more </button>
                        }
                    </div>
                </div>
            </div>
        )
    }
}
